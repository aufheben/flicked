package com.example.aufheben.flic;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.flic.lib.FlicButton;
import io.flic.lib.FlicButtonCallback;
import io.flic.lib.FlicButtonCallbackFlags;
import io.flic.lib.FlicManager;
import io.flic.lib.FlicManagerInitializedCallback;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class MainActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // flic
        FlicManager.setAppCredentials("7c1e64ac-cb94-4f33-a9bf-4d2e9a42e924", "4f075ff2-7f11-45ff-9f56-3855c6c948cd", "Flicked");
        FlicManager.getInstance(this, new FlicManagerInitializedCallback() {

            @Override
            public void onInitialized(FlicManager manager) {
                MainActivity.this.manager = manager;
                manager.initiateGrabButton(MainActivity.this);
            }
        });

        // TTS
        tts = new TextToSpeech(this, this);
        tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onDone(String utteranceId) {
                lastTime = System.currentTimeMillis();
                isSpeaking = false;
            }

            @Override
            public void onError(String utteranceId) {
            }

            @Override
            public void onStart(String utteranceId) {
                isSpeaking = true;
            }
        });

        // twitter
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("rlIjlUkHTnpeMCv4sS22aqTsC")
                .setOAuthConsumerSecret("dfZZk7NdxyOU970JdHqSvJsu8bxRoJsOSnadDiwIXAwpYM0ugI")
                .setOAuthAccessToken("296164711-c3Pn1LlcdakaVfHGZIuJgfj5Wcm7SLvGpWgdY7nD")
                .setOAuthAccessTokenSecret("NkwkqwCgD0Y36XAkCNzPQc6vfLo1GBZTeWhFIeqwRgdXB");
        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter = tf.getInstance();
    }

    @Override
    protected void onDestroy() {
        FlicManager.destroyInstance();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private FlicManager manager;
    private TextToSpeech tts;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        FlicButton button = manager.completeGrabButton(requestCode, resultCode, data);

        if (button != null) {
            Log.d("MainActivity", "Got a button: " + button);
            setButtonCallback(button);
        }
    }

    private void setButtonCallback(FlicButton button) {
        button.removeAllFlicButtonCallbacks();
        button.addFlicButtonCallback(buttonCallback);
        button.setFlicButtonCallbackFlags(FlicButtonCallbackFlags.CLICK_OR_DOUBLE_CLICK_OR_HOLD);
    }

    private int cursorIndex;
    private long lastTime;
    private boolean isSpeaking;
    private String replyToName;
    private String replyToAddress;
    private int mode; // 0 message, 1 twitter, 2 music (TODO)
    private Twitter twitter;
    private List<Status> statuses = null;
    private int twitterIndex;

    private FlicButtonCallback buttonCallback = new FlicButtonCallback() {
        @Override
        public void onButtonSingleOrDoubleClickOrHold(FlicButton button, boolean wasQueued, int timeDiff, boolean isSingleClick, boolean isDoubleClick, boolean isHold) {
            final String text = button + " was " + (isSingleClick ? "single clicked" : isDoubleClick ? "double clicked" : "hold");
            Log.d("FlicButtonCallback", text);

            if (isHold) {
                mode = (mode + 1) % 2;
                if (mode == 1) {
                    try {
                        statuses = twitter.getHomeTimeline();
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                    Log.d("###", "Showing home timeline.");
                    for (Status status : statuses) {
                        Log.d("###", status.getUser().getName() + ":" + status.getText());
                    }

                    tts.speak("Twitter mode", TextToSpeech.QUEUE_FLUSH, null);
                } else {
                    tts.speak("Message mode", TextToSpeech.QUEUE_FLUSH, null);
                }
            } else {
                if (mode == 0) {
                    if (isSingleClick) {
                        if (isSpeaking) {
                            cursorIndex++;
                        } else if (lastTime != 0) {
                            long currentTime = System.currentTimeMillis();
                            if (currentTime - lastTime > 5000) {
                                cursorIndex = 0;
                            } else {
                                cursorIndex++;
                            }
                        }

                        Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);

                        if (cursor.moveToPosition(cursorIndex)) { // must check the result to prevent exception
                            String msg_body = cursor.getString(cursor.getColumnIndex("body"));
                            String address = cursor.getString(cursor.getColumnIndex("address"));
                            String name = getContactName(MainActivity.this, address);
                            String msg = "From " + name + ": " + msg_body;
                            Log.d("MainActivity", msg);

                            HashMap<String, String> tts_hash = new HashMap<String, String>();
                            tts_hash.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "id");
                            tts.speak(msg, TextToSpeech.QUEUE_FLUSH, tts_hash);

                            replyToName = name;
                            replyToAddress = address;
                        } else {
                            // empty box, no SMS
                        }
                        cursor.close();
                    } else {
                        if (replyToAddress != null) {
                            // FIXME: disabled for the demo to save €
                            // SmsManager.getDefault().sendTextMessage(replyToAddress, null, "I'll get back to you later", null, null);
                            tts.speak("Reply message sent to " + replyToName, TextToSpeech.QUEUE_FLUSH, null);
                        }
                    }
                } else {
                    if (isSingleClick) {
                        Status status = statuses.get(twitterIndex);
                        tts.speak(status.getUser().getName() + ":" + status.getText(), TextToSpeech.QUEUE_FLUSH, null);

                        twitterIndex++;
                        if (twitterIndex == statuses.size()) {
                            twitterIndex = 0;
                        }
                    }
                }
            }
        }
    };

    private String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.US);
        } else {
            Log.e("TTS", "Initialization failed");
        }
    }
}
